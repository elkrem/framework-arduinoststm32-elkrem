#
# Detect OS
ifeq ($(OS),Windows_NT)
    CURRENT_OS = WINDOWS
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        CURRENT_OS = LINUX
    endif
    ifeq ($(UNAME_S),Darwin)
        CURRENT_OS = MAC
    endif
endif
# Returns the first argument (typically a directory), if the file or directory
# named by concatenating the first and optionally second argument
# (directory and optional filename) exists
dir_if_exists = $(if $(wildcard $(1)$(2)),$(1))
###################################################
# This is a Makefile for compiling user sketches along with elk core
DIR :=
PROJ_NAME := 
CORE_DIR := STM32F4
BLD := build
ifeq ($(CURRENT_OS), WINDOWS)
PLATFORM_ENV := $(call dir_if_exists,$(USERPROFILE)/Documents/ElkProjects)
else
PLATFORM_ENV := $(call dir_if_exists,$(HOME)/Documents/ElkProjects)
endif
PROJ_ENV := $(PLATFORM_ENV)/$(PROJ_NAME)
PROJ_SRC := $(PROJ_ENV)/src
BLD_PATH := $(PROJ_ENV)/$(BLD)
USER_LIB_PATH := $(PLATFORM_ENV)/libraries
CORE_LIB_PATH := $(CORE_DIR)/libraries

START_UP_FILE := $(CORE_DIR)/system/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc/startup_stm32f411xe.s

LINKER_SCRIPT := $(CORE_DIR)/variants/ELKREM_GENISYS/ldscript.ld
###################################################
# Helper macros

REMOVE  = rm -rf
MV      = mv -f
CAT     = cat
ECHO    = printf
MKDIR   = mkdir -p
###################################################
# Core sources and includes

CORE_AS_SRC = \
		$(wildcard $(CORE_DIR)/cores/arduino/stm32/*.S) \

CORE_C_SRC = \
		$(wildcard $(CORE_DIR)/cores/arduino/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/avr/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/src/proto/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/vendor/nanopb/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/vendor/tinyproto/src/proto/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/stm32/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/stm32/HAL/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/stm32/LL/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/stm32/usb/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/stm32/usb/cdc/*.c) \
		$(wildcard $(CORE_DIR)/cores/arduino/stm32/usb/hid/*.c) \

CORE_CPP_SRC = \
		$(wildcard $(CORE_DIR)/cores/arduino/*.cpp) \
		$(wildcard $(CORE_DIR)/cores/arduino/stm32/*.cpp) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/src/services/blockchain/src/*.cpp) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/src/services/connectivity/src/*.cpp) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/src/services/utility/src/*.cpp) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/src/system/communication/src/*.cpp) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/src/system/main/src/*.cpp) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/src/system/wiring/src/*.cpp) \
		$(wildcard $(CORE_DIR)/cores/arduino/elkrem-sdk/vendor/tinyproto/src/*.cpp) \
		$(wildcard $(CORE_DIR)/variants/ELKREM_GENISYS/*.cpp) \
		

CORE_INCLUDES += \
		-I$(CORE_DIR)/cores/arduino/ \
		-I$(CORE_DIR)/cores/arduino/stm32/ \
		-I$(CORE_DIR)/cores/arduino/stm32/HAL/ \
		-I$(CORE_DIR)/cores/arduino/stm32/LL/ \
		-I$(CORE_DIR)/cores/arduino/stm32/usb/ \
		-I$(CORE_DIR)/cores/arduino/stm32/usb/cdc/ \
		-I$(CORE_DIR)/cores/arduino/stm32/usb/hid/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/proto/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/services/blockchain/inc/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/services/connectivity/inc/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/services/utility/inc/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/system/communication/inc/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/system/proto/inc/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/system/utility/inc/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/system/wiring/inc/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/src/system/main/inc \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/vendor/nanopb/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/vendor/tinyproto/src/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/vendor/tinyproto/src/proto/ \
		-I$(CORE_DIR)/cores/arduino/elkrem-sdk/vendor/tinyproto/src/proto/os/arduino/ \
		-I$(CORE_DIR)/system/Drivers/CMSIS/core/Include/ \
		-I$(CORE_DIR)/system/Drivers/CMSIS/Device/ST/STM32F4xx/Include/ \
		-I$(CORE_DIR)/system/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc/ \
		-I$(CORE_DIR)/system/Drivers/STM32F4xx_HAL_Driver/Inc \
		-I$(CORE_DIR)/system/Drivers/STM32F4xx_HAL_Driver/Src \
		-I$(CORE_DIR)/system/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy/ \
		-I$(CORE_DIR)/system/STM32F4xx/ \
		-I$(CORE_DIR)/variants/ELKREM_GENISYS/\
###################################################
# recursive wildcard function, call with params:
#  - start directory (finished with /) or empty string for current dir
#  - glob pattern
# (taken from http://blog.jgc.org/2011/07/gnu-make-recursive-wildcard-function.html)
rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

# functions used to determine various properties of library
# called with library path. Needed because of differences between library
# layouts in arduino 1.0.x and 1.5.x.
# Assuming new 1.5.x layout when there is "src" subdirectory in main directory
# and library.properties file

# Gets include flags for library
get_library_includes = $(if $(and $(wildcard $(1)/src), $(wildcard $(1)/library.properties)), \
                           -I$(1)/src, \
                           $(addprefix -I,$(1) $(wildcard $(1)/utility)))

# Gets all sources with given extension (param2) for library (path = param1)
# for old (1.0.x) layout looks in . and "utility" directories
# for new (1.5.x) layout looks in src and recursively its subdirectories
get_library_files  = $(if $(and $(wildcard $(1)/src), $(wildcard $(1)/library.properties)), \
                        $(call rwildcard,$(1)/src/,*.$(2)), \
                        $(wildcard $(1)/*.$(2) $(1)/utility/*.$(2)))

# Local sources

LOCAL_C_SRCS    ?= $(wildcard $(PROJ_ENV)/src/*.c)
LOCAL_CPP_SRCS  ?= $(wildcard $(PROJ_ENV)/src/*.cpp)
LOCAL_CC_SRCS   ?= $(wildcard $(PROJ_ENV)/src/*.cc)
LOCAL_PDE_SRCS  ?= $(wildcard $(PROJ_ENV)/src/*.pde)
LOCAL_INO_SRCS  ?= $(wildcard $(PROJ_ENV)/src/*.ino)
LOCAL_AS_SRCS   ?= $(wildcard $(PROJ_ENV)/src/*.S)

SKETCH_C_SRCS ?= $(notdir $(LOCAL_C_SRCS))
SKETCH_CPP_SRCS ?= $(notdir $(LOCAL_CPP_SRCS))
SKETCH_CC_SRCS ?= $(notdir $(LOCAL_CC_SRCS))
SKETCH_PDE_SRCS ?= $(notdir $(LOCAL_PDE_SRCS))
SKETCH_INO_SRCS ?= $(notdir $(LOCAL_INO_SRCS))
SKETCH_AS_SRCS ?= $(notdir $(LOCAL_AS_SRCS))

LOCAL_SRCS      = $(LOCAL_C_SRCS)   $(LOCAL_CPP_SRCS) \
		$(LOCAL_CC_SRCS)   $(LOCAL_PDE_SRCS) \
		$(LOCAL_INO_SRCS) $(LOCAL_AS_SRCS)

SKETCH_SRCS      = $(SKETCH_C_SRCS)   $(SKETCH_CPP_SRCS) \
		$(SKETCH_CC_SRCS)   $(SKETCH_PDE_SRCS) \
		$(SKETCH_INO_SRCS) $(SKETCH_AS_SRCS)

SKETCH_OBJ_FILES = $(SKETCH_C_SRCS:.c=.c.o)   $(SKETCH_CPP_SRCS:.cpp=.cpp.o) \
		$(SKETCH_CC_SRCS:.cc=.cc.o)   $(SKETCH_PDE_SRCS:.pde=.pde.o) \
		$(SKETCH_INO_SRCS:.ino=.ino.o) $(SKETCH_AS_SRCS:.S=.S.o)
SKETCH_OBJS      = $(patsubst %,$(BLD_PATH)/%,$(SKETCH_OBJ_FILES))

CORE_OBJ_FILES = $(CORE_AS_SRC:.S=.S.o) $(CORE_C_SRC:.c=.c.o) $(CORE_CPP_SRC:.cpp=.cpp.o)
CORE_OBJ =  $(patsubst %,$(BLD_PATH)/%,$(CORE_OBJ_FILES))

ifeq ($(words $(LOCAL_SRCS)), 0)
    $(error At least one source file (*.ino, *.pde, *.cpp, *c, *cc, *.S) is needed)
endif

########################################################################
# Determine ARDUINO_LIBS automatically

ifndef ARDUINO_LIBS
    # automatically determine included libraries
    ARDUINO_LIBS += $(filter $(notdir $(wildcard $(PLATFORM_ENV)/libraries/*)), \
        $(shell sed -ne 's/^ *\# *include *[<\"]\(.*\)\.h[>\"]/\1/p' $(LOCAL_SRCS)))
    ARDUINO_LIBS += $(filter $(notdir $(wildcard $(CORE_DIR)/libraries/*)), \
        $(shell sed -ne 's/^ *\# *include *[<\"]\(.*\)\.h[>\"]/\1/p' $(LOCAL_SRCS)))
endif

# General arguments
USER_LIBS      := $(sort $(wildcard $(patsubst %,$(USER_LIB_PATH)/%,$(ARDUINO_LIBS))))
USER_LIB_NAMES := $(patsubst $(USER_LIB_PATH)/%,%,$(USER_LIBS))

# Let user libraries override system ones.
SYS_LIBS       := $(sort $(wildcard $(patsubst %,$(CORE_LIB_PATH)/%,$(filter-out $(USER_LIB_NAMES),$(ARDUINO_LIBS)))))
SYS_LIB_NAMES  := $(patsubst $(CORE_LIB_PATH)/%,%,$(SYS_LIBS))


# Error here if any are missing.
LIBS_NOT_FOUND = $(filter-out $(USER_LIB_NAMES) $(SYS_LIB_NAMES),$(ARDUINO_LIBS))
ifneq (,$(strip $(LIBS_NOT_FOUND)))
        $(error The following libraries specified in ARDUINO_LIBS could not be found (searched USER_LIB_PATH and CORE_LIB_PATH): $(LIBS_NOT_FOUND))
endif

SYS_INCLUDES        := $(foreach lib, $(SYS_LIBS),  $(call get_library_includes,$(lib)))
USER_INCLUDES       := $(foreach lib, $(USER_LIBS), $(call get_library_includes,$(lib)))
LIB_C_SRCS          := $(foreach lib, $(SYS_LIBS),  $(call get_library_files,$(lib),c))
LIB_CPP_SRCS        := $(foreach lib, $(SYS_LIBS),  $(call get_library_files,$(lib),cpp))
LIB_AS_SRCS         := $(foreach lib, $(SYS_LIBS),  $(call get_library_files,$(lib),S))
USER_LIB_CPP_SRCS   := $(foreach lib, $(USER_LIBS), $(call get_library_files,$(lib),cpp))
USER_LIB_C_SRCS     := $(foreach lib, $(USER_LIBS), $(call get_library_files,$(lib),c))
USER_LIB_AS_SRCS    := $(foreach lib, $(USER_LIBS), $(call get_library_files,$(lib),S))
LIB_OBJS            = $(patsubst $(CORE_LIB_PATH)/%.c,$(BLD_PATH)/libs/%.c.o,$(LIB_C_SRCS)) \
                      $(patsubst $(CORE_LIB_PATH)/%.cpp,$(BLD_PATH)/libs/%.cpp.o,$(LIB_CPP_SRCS)) \
                      $(patsubst $(CORE_LIB_PATH)/%.S,$(BLD_PATH)/libs/%.S.o,$(LIB_AS_SRCS))
USER_LIB_OBJS       = $(patsubst $(USER_LIB_PATH)/%.cpp,$(BLD_PATH)/userlibs/%.cpp.o,$(USER_LIB_CPP_SRCS)) \
                      $(patsubst $(USER_LIB_PATH)/%.c,$(BLD_PATH)/userlibs/%.c.o,$(USER_LIB_C_SRCS)) \
                      $(patsubst $(USER_LIB_PATH)/%.S,$(BLD_PATH)/userlibs/%.S.o,$(USER_LIB_AS_SRCS))
###################################################
# Compiler specific options

CC =	arm-none-eabi-gcc
CXX =	arm-none-eabi-g++
ASS =	arm-none-eabi-as
LD =	arm-none-eabi-ld
OBJCOPY =	arm-none-eabi-objcopy
OBJDUMP =	arm-none-eabi-objdump
SIZE =	arm-none-eabi-size
AR = arm-none-eabi-ar

ASFLAGS = 	-x assembler-with-cpp

CCFLAGS = 	-std=gnu11 -Os -mcpu=cortex-m4 -mthumb -ffunction-sections -fdata-sections -Wall -nostdlib

CXXFLAGS =  -std=gnu++14 -Os -mcpu=cortex-m4 -mthumb \
						-fno-threadsafe-statics -fno-rtti -fno-exceptions -ffunction-sections -fdata-sections -Wall -nostdlib

CPPFLAGS = 	-DARDUINO=10808 -DSTM32F411xE -DSTM32F4xx -DHAL_UART_MODULE_ENABLED -DARDUINO_ARCH_STM32 \
						-DSERIAL_TX_BUFFER_SIZE=256 -DSERIAL_RX_BUFFER_SIZE=256 $(CORE_INCLUDES) $(USER_INCLUDES) $(SYS_INCLUDES)

LDFLAGS = 	-Os  -mcpu=cortex-m4 -mthumb --specs=nano.specs -Wl,--gc-sections,--relax  \
				  	-Wl,--start-group -Wl,--whole-archive -lm $(CORE_LIB) -Wl,--no-whole-archive -Wl,--end-group\
						-Wl,-Map=$(BLD_PATH)/$(PROJ_NAME).map 
###################################################
# The name of the main targets

TARGET_HEX = $(BLD_PATH)/$(PROJ_NAME).hex
TARGET_ELF = $(BLD_PATH)/$(PROJ_NAME).elf
TARGET_EEP = $(BLD_PATH)/$(PROJ_NAME).eep
TARGET_BIN = $(BLD_PATH)/$(PROJ_NAME).bin
CORE_LIB   = $(BLD_PATH)/libcore.a

all: $(TARGET_ELF) $(TARGET_HEX) $(TARGET_BIN)

$(TARGET_ELF): 	$(CORE_LIB)
	$(CXX) -o $@ -T $(LINKER_SCRIPT) $(LDFLAGS) 
	$(SIZE) $@

$(TARGET_HEX):	$(TARGET_ELF)
	$(OBJCOPY) -O ihex $< $@

$(TARGET_BIN): $(TARGET_ELF)
	$(OBJCOPY) -O binary $< $@

$(CORE_LIB):	$(CORE_OBJ) $(SKETCH_OBJS) $(USER_LIB_OBJS) $(LIB_OBJS)
		$(AR) rcs $@ $(CORE_OBJ) $(SKETCH_OBJS) $(USER_LIB_OBJS) $(LIB_OBJS)
	
$(BLD_PATH)/%.S.o: %.S
	@$(MKDIR) $(dir $@)
	$(CC) -c $(CPPFLAGS) $(ASFLAGS) $< -o $@

$(BLD_PATH)/%.c.o: %.c
	@$(MKDIR) $(dir $@) 
	$(CC) -c  $(CPPFLAGS) $(CCFLAGS) $< -o $@

$(BLD_PATH)/%.cpp.o: $(PROJ_SRC)/%.cpp
	@$(MKDIR) $(dir $@)
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

$(BLD_PATH)/%.cpp.o: %.cpp
	@$(MKDIR) $(dir $@)
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

$(BLD_PATH)/libs/%.c.o: $(CORE_LIB_PATH)/%.c
	@$(MKDIR) $(dir $@)
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

$(BLD_PATH)/libs/%.cpp.o: $(CORE_LIB_PATH)/%.cpp
	@$(MKDIR) $(dir $@)
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

$(BLD_PATH)/libs/%.S.o: $(CORE_LIB_PATH)/%.S
	@$(MKDIR) $(dir $@)
	$(CC) -c $(CPPFLAGS) $(ASFLAGS) $< -o $@

$(BLD_PATH)/userlibs/%.cpp.o: $(USER_LIB_PATH)/%.cpp
	@$(MKDIR) $(dir $@)
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@

$(BLD_PATH)/userlibs/%.c.o: $(USER_LIB_PATH)/%.c
	@$(MKDIR) $(dir $@)
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

$(BLD_PATH)/userlibs/%.S.o: $(USER_LIB_PATH)/%.S
	@$(MKDIR) $(dir $@)
	$(CC) -c $(CPPFLAGS) $(ASFLAGS) $< -o $@

########################################################################

clean:
	$(REMOVE) $(BLD_PATH)

.PHONY: all clean  