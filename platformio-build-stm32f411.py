# Copyright 2014-present PlatformIO <contact@platformio.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
Arduino

Arduino Wiring-based Framework allows writing cross-platform software to
control devices attached to a wide range of Arduino boards to create all
kinds of creative coding, interactive objects, spaces or physical experiences.

http://www.stm32duino.com
"""


from os.path import isdir, join

from SCons.Script import DefaultEnvironment

env = DefaultEnvironment()
platform = env.PioPlatform()
board = env.BoardConfig()

FRAMEWORK_DIR = join(platform.get_package_dir("framework-arduinoststm32-elkrem"), "STM32F4")

assert isdir(FRAMEWORK_DIR)

variant = board.get("build.variant")
variant_dir = join(FRAMEWORK_DIR, "variants", variant)

ldscript = join(FRAMEWORK_DIR,"variants/ELKREM_GENISYS/ldscript.ld")


env.Append(
    ASFLAGS=["-x", "assembler-with-cpp"],

    CFLAGS=[
        "-std=gnu11"
    ],

    CCFLAGS=[
        "-Os",  # optimize for size
        "-mcpu=%s" % env.BoardConfig().get("build.cpu"),
        "-mthumb",
        "-ffunction-sections",  # place each function in its own section
        "-fdata-sections",
        "-Wall",
        "-nostdlib"
    ],

    CXXFLAGS=[
        "-std=gnu++14",
        "-fno-threadsafe-statics",
        "-fno-rtti",
        "-fno-exceptions"
    ],

    CPPDEFINES=[
      ("ARDUINO",10808),
      ("STM32F411xE"),
      ("STM32F4xx"),
      "HAL_UART_MODULE_ENABLED",
      "ARDUINO_ARCH_STM32",
      ("SERIAL_TX_BUFFER_SIZE",256),
      ("SERIAL_RX_BUFFER_SIZE",256)
    ],

    CPPPATH=[
        join(FRAMEWORK_DIR, "cores", "arduino"),
        join(FRAMEWORK_DIR, "cores", "arduino", "stm32"),
        join(FRAMEWORK_DIR, "cores", "arduino", "stm32", "HAL"),
        join(FRAMEWORK_DIR, "cores", "arduino", "stm32", "LL"),
        join(FRAMEWORK_DIR, "cores", "arduino", "stm32", "usb"),
        join(FRAMEWORK_DIR, "cores", "arduino", "stm32", "usb", "cdc"),
        join(FRAMEWORK_DIR, "cores", "arduino", "stm32", "usb", "hid"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","proto"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","services","blockchain","inc"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","services","connectivity","inc"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","services","utility","inc"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","system","communication","inc"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","system","proto","inc"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","system","utility","inc"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","system","wiring","inc"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","src","system","main","inc"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","vendor","nanopb"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","vendor","tinyproto","src"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","vendor","tinyproto","src","proto"),
        join(FRAMEWORK_DIR, "cores", "arduino","elkrem-sdk","vendor","tinyproto","src","proto","os","arduino"),
        join(FRAMEWORK_DIR, "system", "Drivers", "CMSIS" , "core" , "Include"),
        join(FRAMEWORK_DIR, "system", "Drivers", "CMSIS" , "Device" , "ST" , "STM32F4xx" , "Include"),
        join(FRAMEWORK_DIR, "system", "Drivers", "CMSIS", "Device", "ST", "STM32F4xx", "Source", "Templates", "gcc"),
        join(FRAMEWORK_DIR, "system", "Drivers", "STM32F4xx_HAL_Driver" , "Inc"),
        join(FRAMEWORK_DIR, "system", "Drivers", "STM32F4xx_HAL_Driver" , "Src"),
        join(FRAMEWORK_DIR, "system", "Drivers", "STM32F4xx_HAL_Driver" , "Inc" , "Legacy"),
        join(FRAMEWORK_DIR, "system", "STM32F4xx"),
        join(FRAMEWORK_DIR, "variants", "ELKREM_GENISYS"),
        join(FRAMEWORK_DIR, "system", "Middlewares", "ST",
             "STM32_USB_Device_Library", "Core", "Inc"),
        join(FRAMEWORK_DIR, "system", "Middlewares", "ST",
             "STM32_USB_Device_Library", "Core", "Src")
	],

    LINKFLAGS=[
        "-Os",
        "-mthumb",
        "-mcpu=%s" % env.BoardConfig().get("build.cpu"),
        "--specs=nano.specs",
        "-Wl,--gc-sections,--relax"
	],

    LIBS=["gcc", "m"],

    LIBSOURCE_DIRS=[
        join(FRAMEWORK_DIR, "libraries")
    ]

)


# copy CCFLAGS to ASFLAGS (-x assembler-with-cpp mode)
env.Append(ASFLAGS=env.get("CCFLAGS", [])[:])



# remap ldscript
env.Replace(
		LDSCRIPT_PATH=ldscript,
		_LIBFLAGS="-Wl,--whole-archive ${_stripixes(LIBLINKPREFIX, LIBS, LIBLINKSUFFIX, LIBPREFIXES, LIBSUFFIXES, __env__)} -Wl,--no-whole-archive",
		UPLOADER=join(platform.get_package_dir("tool-elkrem") or "","elkremuploader.py"),
		UPLOADCMD="$PYTHONEXE $UPLOADER -ip $UPLOAD_PORT -b $SOURCE"
	)

#
# Target: Build Core Library
#

libs = []

if "build.variant" in env.BoardConfig():
    env.Append(
        CPPPATH=[variant_dir]
    )
    libs.append(env.BuildLibrary(
        join("$BUILD_DIR", "FrameworkArduinoVariant"),
        variant_dir
    ))

libs.append(env.BuildLibrary(
        join("$BUILD_DIR", "FrameworkArduino"),
        join(FRAMEWORK_DIR, "cores", "arduino")))

env.Prepend(LIBS=libs)